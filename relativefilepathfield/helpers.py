import os
import posixpath

def fullsplit(path, result=None):
    """
    Split a pathname into components (the opposite of os.path.join)
    in a platform-neutral way.
    """
    if result is None:
        result = []
    head, tail = os.path.split(path)
    if head == '':
        return [tail] + result
    if head == path:
        return result
    return fullsplit(head, [tail] + result)

def to_posix(value):
    parts = fullsplit(value)
    return posixpath.join(*parts)

def from_posix(value):
    parts = value.split(posixpath.sep)
    return os.path.join(*parts)
