from __future__ import absolute_import
import os
from django.db.models.fields import FilePathField
from relativefilepathfield import forms
from relativefilepathfield.helpers import from_posix, to_posix

class AbsPath(object):

    def __init__(self, field):
        self.field = field

    def __get__(self, instance=None, owner=None):
        if instance is None:
            raise AttributeError(
                "The '%s' attribute can only be accessed from %s instances."
                % (self.field.name, owner.__name__))

        return lambda: os.path.join(self.field.path, from_posix(instance.__dict__[self.field.name]))

class RelativeFilePathField(FilePathField):
    
    def formfield(self, **kwargs):
        defaults = {
            'form_class': forms.RelativeFilePathField,
        }
        defaults.update(kwargs)
        return super(RelativeFilePathField, self).formfield(**defaults)
    
    def contribute_to_class(self, cls, name):
        super(RelativeFilePathField, self).contribute_to_class(cls, name)
        setattr(cls, 'get_%s_abspath' % self.name, AbsPath(self))

    def get_db_prep_value(self, value, connection, prepared=False):
        value = super(RelativeFilePathField, self).get_db_prep_value(value, connection, prepared)
        if not value:
            return value
        return to_posix(value)

try:
    from south.modelsinspector import add_introspection_rules
    add_introspection_rules([], ["^relativefilepathfield\.fields\.RelativeFilePathField"])
except ImportError: pass
